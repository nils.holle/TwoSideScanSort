from PyPDF2 import PdfFileWriter, PdfFileReader
import sys

output_pdf = PdfFileWriter()

with open(sys.argv[1], "rb") as readfile:
    input_pdf = PdfFileReader(readfile)
    total_pages = input_pdf.getNumPages()

    for page in range(int(total_pages / 2)):
        output_pdf.addPage(input_pdf.getPage(page))
        output_pdf.addPage(input_pdf.getPage(total_pages - page - 1))

    with open(sys.argv[2], "wb") as writefile:
        output_pdf.write(writefile)
